package main

import (
	"crypto/sha1"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"
)

const BLOCKSIZE = 1 << 20

var cpath = flag.String("config", "fido.conf", "Path to the fido Config File")

func main() {
	flag.Parse()
	c, err := ParseConfig(*cpath)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	dgs := make(map[string]*sync.Mutex)
	//Fill the driveGroup Map
	for _, d := range c.Dirs {
		p, err := NewProvider(c.DatabaseHost, d.Name, c.DatabaseUser, c.DatabasePassword)
		if err != nil {
			fmt.Printf("failed to connect to database: %v\n", err)
			os.Exit(3)
		}
		if nil == dgs[d.DriveGroup] {
			dgs[d.DriveGroup] = new(sync.Mutex)
		}
		w := NewRootWorker(d, dgs[d.DriveGroup], p)
		w.Start()
	}
	select {}

	fmt.Printf("Config:\n %+v \n", c)

	// p, err := NewProvider("localhost", "test", "konsti", "slojit")
	// if err != nil {
	// 	log.Fatalln(err)
	// }
	// worker := NewRootWorker("/home/konsti/src", p)
	// worker.Start()
	// worker.Block()
}

type RootWorker struct {
	Path           string
	driveGroupLock *sync.Mutex //To avoid multible workers aktive on the same disk causing seek
	interval       time.Duration
	dbProvider     *Provider
}

func NewRootWorker(cfg watchDir, driveGroupLock *sync.Mutex, provider *Provider) *RootWorker {
	return &RootWorker{
		Path:           cfg.Path,
		interval:       cfg.Interval,
		driveGroupLock: driveGroupLock,
		dbProvider:     provider,
		// files:      make(chan string),
	}
}

//Calls Done on wait Once when all files have been scanned
func (rw *RootWorker) Start() {
	go func() {
		for {
			rw.driveGroupLock.Lock()
			filepath.Walk(rw.Path, rw.handleFile)
			rw.driveGroupLock.Unlock()
			log.Printf("Finished round for %s\n", rw.Path)
			time.Sleep(rw.interval)
		}
	}()
}

func (rw *RootWorker) handleFile(path string, info os.FileInfo, err error) error {
	if info.IsDir() {
		return nil
	}
	path, _ = filepath.Rel(rw.Path, path)
	// log.Println(path)
	f := NewFile(path, rw.Path)
	df, err := rw.dbProvider.GetFile(path)
	if err != nil {
		log.Println(err)
		return err
	}
	if df != nil {
		if c := f.Check(df); c.ok {
			// log.Println("File OK")
			return nil
		} else {
			log.Printf("Broken Blocks: %q\n", c.brokenBlocks)
		}
	}

	if f != nil && df == nil {
		err := rw.dbProvider.InsertFile(f)
		if err != nil {
			log.Println(err)
		}
	}
	return nil
}

type File struct {
	path   string
	size   int64
	mtime  time.Time
	hash   *HashObject
	blocks map[int64]*Block
}

func NewFile(path, root string) *File {

	f, err := os.Open(filepath.Join(root, path))
	defer f.Close()
	if err != nil {
		log.Println(err)
		return nil
	}
	stat, err := f.Stat()
	if err != nil {
		log.Println(err)
		return nil
	}
	nf := File{
		path:   path,
		size:   stat.Size(),
		mtime:  stat.ModTime(),
		blocks: make(map[int64]*Block, stat.Size()/BLOCKSIZE+1), //key: offset
	}

	//Calulate the Hashes
	fhasher := sha1.New()
	buff := make([]byte, BLOCKSIZE)
	var o int64 = 0
	for o < stat.Size() {
		// log.Printf("%s:%d", path, o)
		if n, err := f.ReadAt(buff, o); n > 0 && (err == nil || err == io.EOF) {
			// log.Printf("Read: %d+%d\n", o, n)
			nf.blocks[o] = NewBlock(o, buff[:n])
			m, err := fhasher.Write(buff[:n])
			if m != n || err != nil {
				log.Printf("Hashing failed for %s: %v", path, err)
			}
			o += int64(n)
		} else {
			log.Println(err)
			o += BLOCKSIZE
		}
	}

	nf.hash = &HashObject{fhasher.Sum(make([]byte, 0)), time.Now()}
	return &nf
}

func (f *File) Check(of *File) CheckResult {
	if f.mtime.UTC().Equal(of.mtime.UTC()) || f.size != of.size {
		log.Println("File Changed")
		log.Printf("%v - %v || %v - %v", f.mtime.UTC(), of.mtime.UTC(), f.size, of.size)
		return CheckResult{ok: false}
	}

	if f.hash.Equal(of.hash) {
		return CheckResult{ok: true}
	}
	//We need to do an extended Check
	var brokenBlocks []int64
	log.Println("Performing Block Check")
	for k, v := range f.blocks {
		if !v.hash.Equal(of.blocks[k].hash) {
			brokenBlocks = append(brokenBlocks, k)
		}
	}
	return CheckResult{ok: false, brokenBlocks: brokenBlocks}
}

type Block struct {
	offset int64
	size   int32
	hash   *HashObject
}

func NewBlock(offset int64, buff []byte) *Block {
	h := sha1.Sum(buff)
	b := Block{
		offset: offset,
		size:   int32(len(buff)),
		hash:   &HashObject{h[0:len(h)], time.Now()},
	}
	return &b
}

type HashObject struct {
	sha1      []byte
	generated time.Time
}

func (h *HashObject) Equal(o *HashObject) bool {
	if len(h.sha1) != len(o.sha1) {
		return false
	}
	for i := 0; i < len(h.sha1); i++ {
		if h.sha1[i] != o.sha1[i] {
			return false
		}
	}
	return true
}

type CheckResult struct {
	ok           bool
	brokenBlocks []int64
}
