-- Created by Vertabelo (http://vertabelo.com)
-- Script type: create
-- Scope: [tables, references, sequences, views, procedures]
-- Generated at Thu Feb 26 14:50:44 UTC 2015




-- tables
-- Table: Blocks
CREATE TABLE Blocks (
    offs bigint  NOT NULL,
    file bigint  NOT NULL,
    hash bigint  NOT NULL,
    size int  NULL,
    CONSTRAINT Blocks_pk PRIMARY KEY (offs,file)
);



-- Table: Files
CREATE TABLE Files (
    path text  NOT NULL,
    hash bigint  NOT NULL,
    id bigserial  NOT NULL,
    size bigint  NULL,
    mtime timestamp  NULL,
    CONSTRAINT Files_ak_Path UNIQUE (path) NOT DEFERRABLE  INITIALLY IMMEDIATE ,
    CONSTRAINT Files_pk PRIMARY KEY (id)
);



-- Table: Hash_Objects
CREATE TABLE Hash_Objects (
    id bigserial  NOT NULL,
    sha1 bytea  NULL,
    generated timestamp  NOT NULL,
    CONSTRAINT Hash_Objects_pk PRIMARY KEY (id)
);






-- views
-- View: CompleteFile
CREATE VIEW CompleteFile AS
SELECT f.path AS Path, f.size AS FileSize, f.mtime AS MTime, h.sha1 AS FileHash, b.offs AS Offset, b.size AS BlockSize, bh.sha1 AS BlockHash, bh.generated AS Generated FROM files as f
 LEFT JOIN hash_objects as h ON f.hash = h.id 
 LEFT JOIN blocks b ON b.file = f.id 
 LEFT JOIN hash_objects bh ON b.hash = bh.id
;







-- foreign keys
-- Reference:  Blocks_Files (table: Blocks)


ALTER TABLE Blocks ADD CONSTRAINT Blocks_Files 
    FOREIGN KEY (file)
    REFERENCES Files (id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Blocks_Hash_Objects (table: Blocks)


ALTER TABLE Blocks ADD CONSTRAINT Blocks_Hash_Objects 
    FOREIGN KEY (hash)
    REFERENCES Hash_Objects (id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Files_Hash_Objects (table: Files)


ALTER TABLE Files ADD CONSTRAINT Files_Hash_Objects 
    FOREIGN KEY (hash)
    REFERENCES Hash_Objects (id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;






-- End of file.
