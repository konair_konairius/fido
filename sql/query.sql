SELECT 
	f.path AS Path, 
	f.size AS FileSize,
	f.mtime AS MTime, 
	h.sha256 AS FileHash, 
	b.offs AS Offset, 
	b.size AS BlockSize, 
	bh.sha256 AS BlockHash,
	bh.generated AS Generated 
	  FROM files as f
		LEFT JOIN hash_objects as h ON f.hash = h.id 
		LEFT JOIN blocks b ON b.file = f.id 
		LEFT JOIN hash_objects bh ON b.hash = bh.id
;