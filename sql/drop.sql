-- Created by Vertabelo (http://vertabelo.com)
-- Script type: drop
-- Scope: [tables, references, sequences, views, procedures]
-- Generated at Thu Feb 26 14:51:19 UTC 2015






-- foreign keys
ALTER TABLE Blocks DROP CONSTRAINT Blocks_Files;

ALTER TABLE Blocks DROP CONSTRAINT Blocks_Hash_Objects;

ALTER TABLE Files DROP CONSTRAINT Files_Hash_Objects;




-- views
DROP VIEW CompleteFile;


-- tables
DROP TABLE Blocks;
DROP TABLE Files;
DROP TABLE Hash_Objects;




-- End of file.
