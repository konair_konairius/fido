package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"
)

type config struct {
	DatabaseHost     string
	DatabaseUser     string
	DatabasePassword string
	Dirs             []watchDir
}

func ParseConfig(path string) (*config, error) {
	if path == "" {
		return nil, fmt.Errorf("can't parse empty config path")
	}
	d, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("failed to read config: %v", err)
	}
	var c config
	err = json.Unmarshal(d, &c)
	if err != nil {
		return nil, fmt.Errorf("failed to parse config: %v", err)
	}
	return &c, nil
}

type watchDir struct {
	Name       string //Name used for the Directory, also used as database name
	Path       string
	Interval   time.Duration //Seconds spend between two scanner rounds (Use something big - a few days)
	DriveGroup string        //Multiple Drive Groups are Processed in parallel

}
