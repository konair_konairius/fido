package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

type Provider struct {
	db *sql.DB
}

func NewProvider(host, database, user, password string) (*Provider, error) {
	db, err := sql.Open("postgres", fmt.Sprintf("host=%s dbname=%s user=%s password=%s", host, database, user, password))
	if err != nil {
		// log.Println(err)
		return nil, err
	}

	p := &Provider{db}

	//we want to check if the database realy exists
	r, err := db.Query("SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'public'")
	if err != nil {
		return nil, fmt.Errorf("Failed to check for Database: %v", err)
	}
	var tables int

	for r.Next() {
		r.Scan(&tables)
	}

	if tables == 0 {
		log.Printf("Empty Database Detected... trying to Create Tables")

		err = p.CreateTables()
		if err != nil {
			return nil, err
		}
	}

	return p, nil
}

func (p *Provider) CreateTables() error {
	tx, err := p.db.Begin()
	if err != nil {
		return fmt.Errorf("Failde to Connect to database: %v", err)
	}
	query, err := Asset("sql/create.sql")
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Failed to Load Creation Querry: %v", err)
	}

	_, err = tx.Query(string(query))
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Failed to Create Tables: %v", err)
	}
	tx.Commit()
	return nil
}

func (p *Provider) InsertFile(f *File) error {
	if p.db == nil {
		return fmt.Errorf("Database not Initalized")
	}

	tx, err := p.db.Begin()
	if err != nil {
		return fmt.Errorf("Failed to open Transaction: %v", err)
	}
	//Insert Hash
	hashid, err := p.insertHash(f.hash, tx)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Failed to Insert File Hash: %v", err)
	}
	var fileid int
	err = tx.QueryRow("INSERT INTO files (path, size, mtime, hash) VALUES($1,$2,$3,$4) RETURNING(id);", f.path, f.size, f.mtime.UTC(), hashid).Scan(&fileid)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Failed to Insert File: %v", err)
	}
	for _, b := range f.blocks {
		err := p.insertBlock(fileid, b, tx)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("Failed to Insert Block: %v", err)
		}
	}

	tx.Commit()

	return nil
}

func (p *Provider) insertBlock(f int, b *Block, tx *sql.Tx) error {
	if b == nil {
		return fmt.Errorf("Invalid Block")
	}
	hashid, err := p.insertHash(b.hash, tx)
	if err != nil {
		return err
	}
	resp, err := tx.Query("INSERT INTO blocks (file, hash, size, offs) VALUES ($1,$2,$3,$4);", f, hashid, b.size, b.offset)
	if err != nil {
		return err
	}
	resp.Close()
	return nil
}

func (p *Provider) insertHash(h *HashObject, tx *sql.Tx) (int, error) {
	var id int
	err := tx.QueryRow("INSERT INTO hash_objects (sha1, generated) VALUES ($1,$2) RETURNING(id);", h.sha1, h.generated).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (p *Provider) GetFile(path string) (*File, error) {
	if p.db == nil {
		return nil, fmt.Errorf("database not initailzed")
	}
	resp, err := p.db.Query("SELECT hash, size, mtime, id FROM files WHERE path = $1", path)
	if err != nil {
		return nil, fmt.Errorf("Failed to Query for File: %v", err)
	}
	defer resp.Close()

	var id, hashid, size int64
	var mtime time.Time

	if !resp.Next() {
		log.Printf("New File: %s\n", path)
		return nil, nil
	}

	resp.Scan(&hashid, &size, &mtime, &id)
	hash, err := p.getHash(hashid)
	if err != nil {
		return nil, fmt.Errorf("Failed to get Hash: %v", err)
	}

	blocks, err := p.getBlocks(id)
	if err != nil {
		return nil, fmt.Errorf("Failed to get Blocks: %v", err)
	}

	f := File{
		path:   path,
		size:   size,
		mtime:  mtime,
		hash:   hash,
		blocks: blocks,
	}
	// log.Printf("%+v", f)
	if resp.Next() {
		return nil, fmt.Errorf("This is impossible - Your database is Currupted")
	}

	// log.Println(resp.Columns())
	// for resp.Next() {
	// 	resp.Scan(r)
	// 	log.Printf("Line: %v", r)
	// }
	// log.Printf("Response: %+v\n", resp)
	return &f, nil
}

func (p *Provider) getBlocks(fileid int64) (map[int64]*Block, error) {
	resp, err := p.db.Query("SELECT offs, hash, size FROM blocks WHERE file = $1", fileid)
	if err != nil {
		return nil, fmt.Errorf("Failed to Query for Block: %v", err)
	}
	defer resp.Close()
	bs := make(map[int64]*Block, 0)

	var offset, hashid int64
	var size int32

	for resp.Next() {
		resp.Scan(&offset, &hashid, &size)
		hash, err := p.getHash(hashid)
		if err != nil {
			return nil, err
		}
		bs[offset] = &Block{offset, size, hash}
	}
	return bs, nil
}

func (p *Provider) getHash(id int64) (*HashObject, error) {
	resp, err := p.db.Query("SELECT sha1, generated FROM hash_objects WHERE id = $1", id)
	if err != nil {
		return nil, fmt.Errorf("Failed to Query for HashObject: %v", err)
	}
	defer resp.Close()

	var sha1 []byte
	var generated time.Time

	if !resp.Next() {
		return nil, fmt.Errorf("Hash not found for id %d", id)
	}
	resp.Scan(&sha1, &generated)
	return &HashObject{
		sha1:      sha1,
		generated: generated,
	}, nil
}
